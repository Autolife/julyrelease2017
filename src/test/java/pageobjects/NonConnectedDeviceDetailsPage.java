package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;


public class NonConnectedDeviceDetailsPage {
	
	//The below webelement is not right need to edit this
	@FindBy(how=How.ID,using="deviceDetailsSubmit")
	public static WebElement AddtoBasket;
	//the element not found issue was mainly because not giving proper element idenfier, had declared ID in xpath defining format

	@FindBy(how=How.XPATH,using="//select[@class='accessory-option ng-pristine ng-valid']")
	public static WebElement QuantityDropdown;
	
	@FindBy(how = How.XPATH, using = "//a[@class='close-btn']")
	public static WebElement CloseBtn;
	
	//have to change the below xpath after getting the environment
		@FindBy(how = How.XPATH, using = "//select[@class='ng-pristine ng-valid accessory-option']")
		public static WebElement ColorDropDown;
		
		@FindBy(how = How.XPATH, using = "//select[@class='ng-pristine ng-valid accessory-option']")
		public static WebElement CapacityDropDown;


	
}

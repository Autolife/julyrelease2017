package actionsPerformed;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import GlobalActions.Screenshots;
import helpers.Environment;




public class OrderConfirmationPageActions extends Environment {
	static Logger log = Logger.getLogger("devpinoyLogger");

	public static void OrderConfirmationPageSections() 
	{
			log.debug("Below are order confirmation page details");
			log.debug("The details are "+pageobjects.OrderConfirmationPage.OrderconfirmationMessage.getText());
			log.debug("...................................");
			log.debug("The details are "+pageobjects.OrderConfirmationPage.deliverySection.getText());
			log.debug("...................................");
			log.debug("The details are "+pageobjects.OrderConfirmationPage.imsMessage.getText());
			System.out.println("The details are "+pageobjects.OrderConfirmationPage.OrderconfirmationMessage.getText());
			System.out.println("...................................");
			System.out.println(pageobjects.OrderConfirmationPage.deliverySection.getText());
			System.out.println("...................................");
			System.out.println(pageobjects.OrderConfirmationPage.imsMessage.getText());
			
	}
	
	  
	  public static void gettitlepage()
	  {
		  
		   System.out.println(driver.getTitle());
			  log.debug("The Page Title is "+driver.getTitle());
			  try
			  {
			  String title = driver.getTitle();
			  Assert.assertEquals(title, "O2 | Congratulations � Order Confirmed" );
			  }
			  catch(Exception e)
			  {
				  Assert.fail("The title is not matching, Probable landed on incorrect page");
			  }
		     
	   }
	  
	
	public static void MessageDisplayed() {
		System.out.println("This is order confirmation page and the message in this page is as below......");
		log.debug("This is order confirmation/information page and the message in this page is as above......");

		try {
			List<WebElement> outercontainer = driver.findElements(By.xpath("//*[@id='order-number']"));
			log.debug("Trying to find the Element for order number using element identifier");

			List<WebElement> DataContainer = outercontainer.get(0).findElements(By.xpath("//*[@id='order-number']"));

			for (int i = 0; i <= DataContainer.size(); i++) {

				System.out.println(DataContainer.get(i).getText());
				log.debug(DataContainer.get(i).getText());
			}
		} catch (IndexOutOfBoundsException e) {

		}
	}
	
	public static void MessageDisplayedFreeSim() {
		System.out.println("This is order confirmation page and the message in this page is as below......");
		log.debug("This is order confirmation/information page and the message in this page is as above......");

		System.out.println(driver.findElement(By.xpath("//*[@class='order-confirmation']")).getText());
		log.debug(driver.findElement(By.xpath("//*[@class='order-confirmation']")).getText());
		
	}
	
	public static void Survey()
	{
	try {
		Environment.driver.switchTo().frame("edr_l_first");
		System.out.println("********We are switch to the iframe*******");
		log.debug("Popup has appeared on the screen, Hence trying to close the survey");
		Screenshots.screennewPics();
		// Saying no to survey
		driver.findElement(By.xpath("//a[@id='no']/span")).click();
		log.debug("Closing the popup by saying No to Survey");
		System.out.println("*******Saying no to survey*******");
		System.out.println("*********Existing the popups present in iframe***************");
		log.debug("Exiting the Survey");
		Environment.driver.switchTo().defaultContent();
		Thread.sleep(3000);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		System.out.println("There are no survey pop-ups available, so it should be fine");
	} 
	}
	
	  public static void VolteMessageDisplayed() 
	  {
		  System.out.println("The Volte message in this page is as below......");
		  try
		  {
		  List<WebElement> outercontainer = driver.findElements(By.xpath("//*[@id='imsMessage']"));

			List<WebElement> DataContainer = outercontainer.get(0).findElements(By.xpath("//*[@id='imsMessage']"));
				        
				      
				        for (int i=0; i<=DataContainer.size();i++)
				        {
				        			            
				        	System.out.println(DataContainer.get(i).getText());			        
				        }
		  }
			catch (IndexOutOfBoundsException e) 
		  {
				
		  } 
		  
		  
	  
	  
	  
	  }
		     

	  }
	 
	  


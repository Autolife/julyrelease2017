Feature: Order Pay as you Go Free Sim for Tablets- Negative test

  @DemoeComJuly2017
  Scenario Outline: Pay As you Go Tablet Sim E2E order Validatio
    Given I am an CFA user and Lands on shop page
    And navigate to Pay as you Go Sims page
    And Choose desired sim by selecting relevant <Tab>
    And Verify user has selected relevant <Tab>
    And Select a required <Sim>
    And Enter relevant details in Delivery page by providing valid <Firstname>, <Surname>
    And Choose to Order Sim
    Then order confirmation is displayed

    Examples: 
      | Firstname | Surname   | Tab         | Sim  |
      | TEST      | DONOTSEND | Accessories | Ipad |

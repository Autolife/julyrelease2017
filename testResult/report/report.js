$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("TestCase1_OrderingPayAsYouGoSim_Positive.feature");
formatter.feature({
  "line": 1,
  "name": "Order Pay as you Go Free Sim for Tablets",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Pay As you Go Tablet Sim E2E order Validation",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets;pay-as-you-go-tablet-sim-e2e-order-validation",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@DemoeComJuly2017"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am an CFA user and Lands on shop page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "navigate to Pay as you Go Sims page",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Choose desired sim by selecting relevant \u003cTab\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Verify user has selected relevant \u003cTab\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Select a required \u003cSim\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Enter relevant details in Delivery page by providing valid \u003cFirstname\u003e, \u003cSurname\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Choose to Order Sim",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "order confirmation is displayed",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets;pay-as-you-go-tablet-sim-e2e-order-validation;",
  "rows": [
    {
      "cells": [
        "Firstname",
        "Surname",
        "Tab",
        "Sim"
      ],
      "line": 15,
      "id": "order-pay-as-you-go-free-sim-for-tablets;pay-as-you-go-tablet-sim-e2e-order-validation;;1"
    },
    {
      "cells": [
        "TEST",
        "DONOTSEND",
        "iPads and tablets",
        "Ipad"
      ],
      "line": 16,
      "id": "order-pay-as-you-go-free-sim-for-tablets;pay-as-you-go-tablet-sim-e2e-order-validation;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 55408313484,
  "status": "passed"
});
formatter.scenario({
  "line": 16,
  "name": "Pay As you Go Tablet Sim E2E order Validation",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets;pay-as-you-go-tablet-sim-e2e-order-validation;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@DemoeComJuly2017"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am an CFA user and Lands on shop page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "navigate to Pay as you Go Sims page",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Choose desired sim by selecting relevant iPads and tablets",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Verify user has selected relevant iPads and tablets",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Select a required Ipad",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Enter relevant details in Delivery page by providing valid TEST, DONOTSEND",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Choose to Order Sim",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "order confirmation is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.i_am_an_CFA_user_and_Lands_on_shop_page()"
});
formatter.result({
  "duration": 674350095,
  "status": "passed"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.navigate_to_PAYG_Sims_page()"
});
formatter.result({
  "duration": 33341502345,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "iPads and tablets",
      "offset": 41
    }
  ],
  "location": "E2EOrderPlaced_Steps.ChooseSim(String)"
});
formatter.result({
  "duration": 2330706590,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "iPads and tablets",
      "offset": 34
    }
  ],
  "location": "E2EOrderPlaced_Steps.VerifyUserOnTab(String)"
});
formatter.result({
  "duration": 2069876190,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Ipad",
      "offset": 18
    }
  ],
  "location": "E2EOrderPlaced_Steps.SelectPayGSim(String)"
});
formatter.result({
  "duration": 14021891368,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "TEST",
      "offset": 59
    },
    {
      "val": "DONOTSEND",
      "offset": 65
    }
  ],
  "location": "E2EOrderPlaced_Steps.PayGFreeSimsDeliveryPage(String,String)"
});
formatter.result({
  "duration": 8170800812,
  "status": "passed"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.OderSimPayGSimFree()"
});
formatter.result({
  "duration": 28510935959,
  "status": "passed"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.OrderConfirmationPage()"
});
formatter.result({
  "duration": 5384899316,
  "status": "passed"
});
formatter.after({
  "duration": 12305239194,
  "status": "passed"
});
formatter.uri("TestCase2_OrderingPayAsYouGoSim_Negative.feature");
formatter.feature({
  "line": 1,
  "name": "Order Pay as you Go Free Sim for Tablets- Negative test",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "Pay As you Go Tablet Sim E2E order Validatio",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test;pay-as-you-go-tablet-sim-e2e-order-validatio",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@DemoeComJuly2017"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am an CFA user and Lands on shop page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "navigate to Pay as you Go Sims page",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Choose desired sim by selecting relevant \u003cTab\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Verify user has selected relevant \u003cTab\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Select a required \u003cSim\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Enter relevant details in Delivery page by providing valid \u003cFirstname\u003e, \u003cSurname\u003e",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Choose to Order Sim",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "order confirmation is displayed",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test;pay-as-you-go-tablet-sim-e2e-order-validatio;",
  "rows": [
    {
      "cells": [
        "Firstname",
        "Surname",
        "Tab",
        "Sim"
      ],
      "line": 15,
      "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test;pay-as-you-go-tablet-sim-e2e-order-validatio;;1"
    },
    {
      "cells": [
        "TEST",
        "DONOTSEND",
        "Accessories",
        "Ipad"
      ],
      "line": 16,
      "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test;pay-as-you-go-tablet-sim-e2e-order-validatio;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.before({
  "duration": 76313081333,
  "error_message": "org.openqa.selenium.NoSuchWindowException: no such window: target window already closed\nfrom unknown error: web view not found\n  (Session info: chrome\u003d58.0.3029.110)\n  (Driver info: chromedriver\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233),platform\u003dWindows NT 6.1.7601 SP1 x86) (WARNING: The server did not provide any stacktrace information)\nCommand duration or timeout: 5.76 seconds\nBuild info: version: \u00273.0.1\u0027, revision: \u00271969d75\u0027, time: \u00272016-10-18 09:49:13 -0700\u0027\nSystem info: host: \u0027LASST266393\u0027, ip: \u002710.249.24.12\u0027, os.name: \u0027Windows 7\u0027, os.arch: \u0027x86\u0027, os.version: \u00276.1\u0027, java.version: \u00271.8.0_121\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities [{applicationCacheEnabled\u003dfalse, rotatable\u003dfalse, mobileEmulationEnabled\u003dfalse, networkConnectionEnabled\u003dfalse, chrome\u003d{chromedriverVersion\u003d2.29.461591 (62ebf098771772160f391d75e589dc567915b233), userDataDir\u003dC:\\Users\\vMalalu1\\AppData\\Local\\Temp\\scoped_dir11208_26463}, takesHeapSnapshot\u003dtrue, pageLoadStrategy\u003dnormal, databaseEnabled\u003dfalse, handlesAlerts\u003dtrue, hasTouchScreen\u003dfalse, version\u003d58.0.3029.110, platform\u003dXP, browserConnectionEnabled\u003dfalse, nativeEvents\u003dtrue, acceptSslCerts\u003dtrue, locationContextEnabled\u003dtrue, webStorageEnabled\u003dtrue, browserName\u003dchrome, takesScreenshot\u003dtrue, javascriptEnabled\u003dtrue, cssSelectorsEnabled\u003dtrue, unexpectedAlertBehaviour\u003d}]\nSession ID: 896c8600cf9b58ed9a1bd31607c1a8ef\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance0(Native Method)\r\n\tat sun.reflect.NativeConstructorAccessorImpl.newInstance(NativeConstructorAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingConstructorAccessorImpl.newInstance(DelegatingConstructorAccessorImpl.java:45)\r\n\tat java.lang.reflect.Constructor.newInstance(Constructor.java:423)\r\n\tat org.openqa.selenium.remote.ErrorHandler.createThrowable(ErrorHandler.java:216)\r\n\tat org.openqa.selenium.remote.ErrorHandler.throwIfResponseFailed(ErrorHandler.java:168)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:635)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:658)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteWebDriverOptions.deleteAllCookies(RemoteWebDriver.java:730)\r\n\tat steps.Hooks.openBrowser(Hooks.java:57)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\r\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.lang.reflect.Method.invoke(Method.java:498)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\r\n\tat cucumber.runtime.java.JavaHookDefinition.execute(JavaHookDefinition.java:60)\r\n\tat cucumber.runtime.Runtime.runHookIfTagsMatch(Runtime.java:224)\r\n\tat cucumber.runtime.Runtime.runHooks(Runtime.java:212)\r\n\tat cucumber.runtime.Runtime.runBeforeHooks(Runtime.java:202)\r\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:40)\r\n\tat cucumber.runtime.junit.ExecutionUnitRunner.run(ExecutionUnitRunner.java:102)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ExamplesRunner.run(ExamplesRunner.java:59)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:128)\r\n\tat org.junit.runners.Suite.runChild(Suite.java:27)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.ScenarioOutlineRunner.run(ScenarioOutlineRunner.java:53)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:63)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:18)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:70)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:95)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:38)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.internal.runners.statements.RunBefores.evaluate(RunBefores.java:26)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.api.junit.Cucumber.run(Cucumber.java:100)\r\n\tat testRunner.CucumberRunner.run(CucumberRunner.java:64)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:137)\r\n\tat org.junit.runner.JUnitCore.run(JUnitCore.java:115)\r\n\tat org.testng.junit.JUnit4TestRunner.start(JUnit4TestRunner.java:82)\r\n\tat org.testng.junit.JUnit4TestRunner.run(JUnit4TestRunner.java:70)\r\n\tat org.testng.TestRunner$1.run(TestRunner.java:675)\r\n\tat org.testng.TestRunner.runJUnitWorkers(TestRunner.java:975)\r\n\tat org.testng.TestRunner.privateRunJUnit(TestRunner.java:706)\r\n\tat org.testng.TestRunner.run(TestRunner.java:607)\r\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:387)\r\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:382)\r\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:340)\r\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:289)\r\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:52)\r\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:86)\r\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1293)\r\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1218)\r\n\tat org.testng.TestNG.runSuites(TestNG.java:1133)\r\n\tat org.testng.TestNG.run(TestNG.java:1104)\r\n\tat org.apache.maven.surefire.testng.TestNGExecutor.run(TestNGExecutor.java:132)\r\n\tat org.apache.maven.surefire.testng.TestNGDirectoryTestSuite.executeSingleClass(TestNGDirectoryTestSuite.java:112)\r\n\tat org.apache.maven.surefire.testng.TestNGDirectoryTestSuite.execute(TestNGDirectoryTestSuite.java:99)\r\n\tat org.apache.maven.surefire.testng.TestNGProvider.invoke(TestNGProvider.java:147)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.invokeProviderInSameClassLoader(ForkedBooter.java:290)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:242)\r\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:121)\r\n",
  "status": "failed"
});
formatter.scenario({
  "line": 16,
  "name": "Pay As you Go Tablet Sim E2E order Validatio",
  "description": "",
  "id": "order-pay-as-you-go-free-sim-for-tablets--negative-test;pay-as-you-go-tablet-sim-e2e-order-validatio;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@DemoeComJuly2017"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am an CFA user and Lands on shop page",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "navigate to Pay as you Go Sims page",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "Choose desired sim by selecting relevant Accessories",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 8,
  "name": "Verify user has selected relevant Accessories",
  "matchedColumns": [
    2
  ],
  "keyword": "And "
});
formatter.step({
  "line": 9,
  "name": "Select a required Ipad",
  "matchedColumns": [
    3
  ],
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "Enter relevant details in Delivery page by providing valid TEST, DONOTSEND",
  "matchedColumns": [
    0,
    1
  ],
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "Choose to Order Sim",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "order confirmation is displayed",
  "keyword": "Then "
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.i_am_an_CFA_user_and_Lands_on_shop_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.navigate_to_PAYG_Sims_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Accessories",
      "offset": 41
    }
  ],
  "location": "E2EOrderPlaced_Steps.ChooseSim(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Accessories",
      "offset": 34
    }
  ],
  "location": "E2EOrderPlaced_Steps.VerifyUserOnTab(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "Ipad",
      "offset": 18
    }
  ],
  "location": "E2EOrderPlaced_Steps.SelectPayGSim(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "arguments": [
    {
      "val": "TEST",
      "offset": 59
    },
    {
      "val": "DONOTSEND",
      "offset": 65
    }
  ],
  "location": "E2EOrderPlaced_Steps.PayGFreeSimsDeliveryPage(String,String)"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.OderSimPayGSimFree()"
});
formatter.result({
  "status": "skipped"
});
formatter.match({
  "location": "E2EOrderPlaced_Steps.OrderConfirmationPage()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 16420031104,
  "status": "passed"
});
});